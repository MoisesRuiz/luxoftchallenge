import { type Page, type Locator } from '@playwright/test';
import basePage from './base.page';

export default class SearchPage extends basePage {
    page: Page;
    switchMode: Locator;
    originInput: Locator;
    destinationInput: Locator;
    returnDateInput: Locator;
    departureDateInput: Locator;
    passengerField: Locator;
    incrementAdultPassengers: Locator;
    submitButton: Locator;
    noReturnTicketButton: Locator;
    closePassengerButton: Locator;
    ticketPrice: Locator;
    departureDate: Locator;
    originLabel: Locator;
    destinationLabel: Locator;

    constructor(page: Page) {
        super();
        this.page = page;

        // Initial Elements  
        this.switchMode = page.locator('div.navbar__control label');
        this.originInput = page.locator('input[data-test-id="origin-autocomplete-field"]');
        this.originLabel = page.locator('div[data-test-id="autocomplete-origin"] span');
        this.destinationInput = page.locator('input#destination');
        this.destinationLabel = page.locator('div[data-test-id="autocomplete-destination"] span');
        this.departureDateInput = page.locator('div.trip-duration__input-wrapper.--departure')
        this.returnDateInput = page.locator('input[placeholder="Return"]');
        this.passengerField = page.locator('div[data-test-id="passengers-field"]');
        this.incrementAdultPassengers = page.locator('div[data-test-id="passengers-adults-field"] a.--increment');
        this.noReturnTicketButton = page.locator('button[data-test-id="no-return-ticket"]');
        this.submitButton = page.locator('button[data-test-id="form-submit"]');
        this.closePassengerButton = page.locator("button.button_2e34a4d");
        this.departureDate = page.locator("div[aria-label='Sat Jul 30 2022']")


        this.ticketPrice = page.locator('div.ticket-desktop__price');
    }

    async navigate(url: string) {
        await this.page.goto(url);
    }

    async enableNightMode() {
        await this.switchMode.click();
    }

    async searchFlight(origin: string, destination: string) {
        await this.typeTextElement(this.originInput, origin);
        await this.typeTextElement(this.destinationInput, destination);
        await this.clickElement(this.departureDateInput);
        await this.clickElement(this.departureDate);

        await this.clickElement(this.passengerField);
        await this.clickElement(this.incrementAdultPassengers);
        await this.keyboardAction(this.page, "Escape");
        await this.clickElement(this.submitButton);
        await this.expectUrlToContainText(this.page, "https://www.aviasales.com/?params=JFK")
        await this.expectToBeVisible(this.ticketPrice);
        await this.validateAllElementsText();
    }

    async validateAllElementsText(){
        await this.expectToHaveText(this.originLabel, "JFK");
        await this.expectToHaveText(this.destinationLabel, "BER");
        
    }
}