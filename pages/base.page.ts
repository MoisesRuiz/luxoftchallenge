import { expect, Page, type Locator } from "@playwright/test";

export default class basePage {
    async clickElement(element: Locator) {
        await element.click();
    }

    async typeTextElement(element: Locator, text: string) {
        await element.type(text);
    }

    async keyboardAction(page: Page, action: any) {
        await page.keyboard.press(action);
    }

    async expectToHaveText(element: Locator, text: string) {
        expect(element).toHaveText(text);
    }

    async expectToBeVisible(element: Locator) {
        expect(element).toBeVisible;
    }

    async expectUrlToContainText(page: Page, text: string) {
        let currentUrl = page.url();
        expect(currentUrl).toContain(text);
    }
}