import { Browser, Page, test } from '@playwright/test';
import pw  from 'playwright'; 
import SearchPage from '../pages/search.page';

let searchPage: SearchPage;
let browser: Browser;
let page: Page;

test.beforeEach(async () => {
  // Runs before each test and signs in each page.
  browser = await pw.chromium.launch({headless: false});
  page = await browser.newPage();
  searchPage = new SearchPage(page);
  await searchPage.navigate('https://www.aviasales.com/');
  page.waitForLoadState('domcontentloaded')
});

test.afterEach(async () => {
  browser.close();
});

test('Switch to Night Mode', async () => {
  await searchPage.enableNightMode();
});

test('Search for a New Flight', async () => {
  await searchPage.searchFlight('John F. Kennedy International Airport', "Berlin");
});





